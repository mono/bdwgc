/*
 * Test that overwrite error detection works reasonably.
 */
#define GC_DEBUG
#include "gc.h"

#include <stdio.h>

#define COUNT 7000
#define SIZE  40

char * A[COUNT];

int main(void)
{
  int i;
  char *p;

  GC_INIT();

  for (i = 0; i < COUNT; ++i) {
     A[i] = p = (char*)GC_MALLOC(SIZE);

     if (i%3000 == 0) GC_gcollect();

/* out of range access is intentional */
#if defined(__GNUC__) && !defined(__clang__)
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Warray-bounds"
# pragma GCC diagnostic ignored "-Wstringop-overflow"
#endif

     if (i%5678 == 0 && p != 0) p[SIZE + i/2000] = 42;

#if defined(__GNUC__) && !defined(__clang__)
# pragma GCC diagnostic pop
#endif
  }
  return 0;
}
